$(document).ready(function(){
	var miLista = $("#miLista");
	var busqueda = $("#busqueda");

	$("#btn-buscar").on("click", function(){

		const palabra = $('#busqueda').val();
		console.log('Palabra a buscar: '+palabra);
		alert('Vamos a buscar: '+palabra);
		/*PONGA AQUI SU CÓDIGO*/
		// Reutilice bastante codigo, mejor dicho "adapte" el codigo encontrado en el archivo catalogo.js
		 
		/* Modificaciones:	
			[linea 22] url:"con link anterior" => url: "http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=3356865d41894a2fa9bfa84b2b5f59bb&query="+palabra,
			[linea 28, 34 y 38] (elemento) => (palabra)
			[linea 30] <div => <li 
			[linea 27, 29, 41, 50 y 51] listaPeliculas => miLista
			[linea 38] eliminacion de la funcion crearModal 
		*/ 
		
					$.ajax({
						url: "http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=3356865d41894a2fa9bfa84b2b5f59bb&query="+palabra,
						success: function(respuesta) {
			
							setTimeout(function () {
								console.log(respuesta);
								miLista.empty();
								$.each(respuesta.results, function(index, palabra) {
									miLista.append(
										'<li class="media text-muted pt-3">'
										+  '<img style="max-width:50px;" class="mr-2 rounded" src="https://image.tmdb.org/t/p/w500' + palabra.poster_path + '"></img>'	
										+  '<p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">'
										+    '<strong class="d-block text-gray-dark" data-toggle="modal" data-target="#modal'+index+'">'+palabra.title+'</strong>'
										+    palabra.overview
										+  '</p>'
										+'</li>'
										);  
									//crearModal(index, palabra.overview);
						
								});
								miLista.slideDown("slow");
							}, 3000);
			
						},
						error: function() {
							console.log("No se ha podido obtener la información");
						},
						beforeSend: function() { 
							console.log('CARGANDO');
							miLista.empty();
							miLista.append('<div class="text-center"><img src="images/loading.gif" /></div>');
						},
					});
	});			

});
